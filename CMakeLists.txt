cmake_minimum_required(VERSION 3.10)
project(gauss_cl CXX)
set(CMAKE_CXX_STANDARD 17)

find_package(OpenCV REQUIRED CONFIG COMPONENTS core imgproc imgcodecs)
find_package(OpenCL REQUIRED COMPONENTS OpenCL)

add_executable(gauss_cl src/gauss_cl.cc)
target_link_libraries(gauss_cl OpenCL::OpenCL opencv_core opencv_imgcodecs)

