#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>

struct my_cl_context {
  cl_platform_id platform_id;
  cl_device_id device_id;
  cl_context context;
  cl_command_queue cq;
};

cl_int createMyClContext(struct my_cl_context* ctx) {
  cl_int err;
  err = clGetPlatformIDs(1, &ctx->platform_id, nullptr);
  if (err != CL_SUCCESS) return err;

  err = clGetDeviceIDs(ctx->platform_id, CL_DEVICE_TYPE_GPU, 1, &ctx->device_id,
                       nullptr);
  if (err != CL_SUCCESS) return err;

  ctx->context =
      clCreateContext(nullptr, 1, &ctx->device_id, nullptr, nullptr, &err);
  if (err != CL_SUCCESS) return err;

  ctx->cq = clCreateCommandQueueWithProperties(ctx->context, ctx->device_id,
                                               nullptr, &err);
  if (err != CL_SUCCESS) {
    clReleaseContext(ctx->context);
  }
  return err;
};

cl_int destroyMyClContext(const struct my_cl_context* ctx) {
  clReleaseCommandQueue(ctx->cq);
  return clReleaseContext(ctx->context);
}


cl_program buildProgramFromString(const struct my_cl_context* ctx,
                                     const char* source,
                                     const char* defines,
                                     cl_int* err) {
  if (ctx == nullptr) {
    *err = CL_BUILD_ERROR;
    return nullptr;
  }

  if (err == nullptr) {
    return nullptr;
  }

  cl_program program =
      clCreateProgramWithSource(ctx->context, 1, &source, nullptr, err);
  if (*err != CL_SUCCESS) return nullptr;

  *err = clBuildProgram(program, 0, nullptr, defines, nullptr, nullptr);
  if (*err != CL_SUCCESS) {
    size_t err_log_len = 0;
    char* err_log;

    cl_int err2 = clGetProgramBuildInfo(program, ctx->device_id, CL_PROGRAM_BUILD_LOG,
                                        0, nullptr, &err_log_len);
    if (err2 != CL_SUCCESS) goto release_program;

    err_log = static_cast<char *>(malloc(err_log_len * sizeof(char)));
    if (!err_log) goto release_program;
    err2 = clGetProgramBuildInfo(program, ctx->device_id, CL_PROGRAM_BUILD_LOG,
                                 err_log_len, err_log, nullptr);
    if (err2 != CL_SUCCESS) goto free_string;

    printf("CL program build failed with errors: %s\n", err_log);

  free_string:
    free(err_log);
  release_program:
    clReleaseProgram(program);
    program = nullptr;
  }

  return program;
}

static const char *const SOURCE = R"(
    inline int safe_index(int2 size, int2 pos) {
      pos = min(max((int2)(0, 0), pos), size - 1);
      return pos.y * size.x + pos.x;
    }


    __constant float3 KERNEL[3] = {
      (float3)(1.0f, 1.0f, 1.0f),
      (float3)(1.0f, 1.0f, 1.0f),
      (float3)(1.0f, 1.0f, 1.0f)
    };

    __kernel void convolve3x3(const int2 size,
                              __global const uchar* input,
                              __global uchar* output) {
      const int2 pos = {get_global_id(0), get_global_id(1)};
      if (size.x <= pos.x || size.y <= pos.y) return;
      float sum = 0.0f;
      for (int dy = -1; dy <= 1; dy++) {
          const float3 row = {
            input[safe_index(size, pos + (int2)(-1, dy))],
            input[safe_index(size, pos + (int2)(0, dy))],
            input[safe_index(size, pos + (int2)(1, dy))],
          };
          sum += dot(KERNEL[1 + dy], row);
      }
      const int i_center = pos.y * size.x + pos.x;
      sum /= (float)(3 * 3);
      output[i_center] = convert_uchar_sat_rte(sum);
})";

int main() {
  struct my_cl_context ctx = {nullptr};
  cl_program program;
  cl_kernel kernel;
  cl_mem buffer_in, buffer_out;
  cl_event kernel_event = nullptr;
  cl_event read_event = nullptr;
  cl_int err = CL_SUCCESS;

  cv::Mat image = cv::imread("../iss.jpg");
  if (image.empty() || image.type() != CV_8UC3) {
    printf("Could not load image\n");
    return 1;
  }
  cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
  cv::imwrite("in_grayscale.png", image);

  const cv::Size image_size = image.size();
  const size_t work_group_size[2] = {8, 8};
  const size_t problem_size[2] = {static_cast<size_t>(image_size.width),
                                  static_cast<size_t>(image_size.height)};
  const size_t work_size[2] = {(problem_size[0] + work_group_size[0] - 1) /
                               work_group_size[0] * work_group_size[0],
                               (problem_size[1] + work_group_size[1] - 1) /
                               work_group_size[1] * work_group_size[1]};

  err = createMyClContext(&ctx);
  if (err != CL_SUCCESS) {
    printf("Could not create context (CL err %d)\n", err);
    goto done;
  }

  program = buildProgramFromString(&ctx, SOURCE, nullptr, &err);
  if (program == nullptr) {
    printf("Could not create program (CL err %d)\n", err);
    goto release_context;
  }

  kernel = clCreateKernel(program, "convolve3x3", &err);
  if (err != CL_SUCCESS) {
    printf("Could not find kernel (CL err %d)\n", err);
    goto release_program;
  }

  buffer_in =
      clCreateBuffer(ctx.context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                     image.size().area(), image.data, &err);
  if (err != CL_SUCCESS) {
    printf("Could not allocate input buffer (CL err %d)\n", err);
    goto release_kernel;
  }

  buffer_out =
      clCreateBuffer(ctx.context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
                     image.size().area(), nullptr, &err);
  if (err != CL_SUCCESS) {
    printf("Could not allocate output buffer (CL err %d)\n", err);
    goto release_in_buffer;
  }

  static_assert(sizeof(cv::Size) == sizeof(cl_int2));
  err = clSetKernelArg(kernel, 0, sizeof(cl_int2), &image_size);
  if (err != CL_SUCCESS) {
    printf("Could not set zeroth argument (CL err %d)\n", err);
    goto release_out_buffer;
  }
  err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &buffer_in);
  if (err != CL_SUCCESS) {
    printf("Could not set first argument (CL err %d)\n", err);
    goto release_out_buffer;
  }
  err = clSetKernelArg(kernel, 2, sizeof(cl_mem), &buffer_out);
  if (err != CL_SUCCESS) {
    printf("Could not set second argument (CL err %d)\n", err);
    goto release_out_buffer;
  }

  err = clEnqueueNDRangeKernel(ctx.cq, kernel, 2, nullptr, work_size,
                               work_group_size, 0, nullptr, &kernel_event);
  if (err != CL_SUCCESS) {
    printf("Could not enqueue kernel (CL err %d)\n", err);
    goto release_out_buffer;
  }

  err = clEnqueueReadBuffer(ctx.cq, buffer_out, false, 0, image.size().area(),
                            image.data, 1, &kernel_event, &read_event);
  if (err != CL_SUCCESS) {
    printf("Could not enqueue buffer read (CL err %d)\n", err);
    goto release_kernel_event;
  }

  err = clWaitForEvents(1, &read_event);
  if (err != CL_SUCCESS) {
    printf("Could not wait for event (CL err %d)\n", err);
    goto release_read_event;
  }

  cv::imwrite("out.png", image);

release_read_event:
  clReleaseEvent(read_event);
release_kernel_event:
  clReleaseEvent(kernel_event);
release_out_buffer:
  clReleaseMemObject(buffer_out);
release_in_buffer:
  clReleaseMemObject(buffer_in);
release_kernel:
  clReleaseKernel(kernel);
release_program:
  clReleaseProgram(program);
release_context:
  destroyMyClContext(&ctx);
done:
  return err == CL_SUCCESS ? 0 : 1;
}
